import pytest
from fastapi import HTTPException
from employeers_bd import EmployeeDB
from auth_service import AuthService
from models import Employee
from salary_service import SalaryService

employees_db = EmployeeDB()
auth_service = AuthService()
salary_service = SalaryService(employees_db, auth_service)


def test_get_salary_valid_token():
    employee = Employee(1, "John Doe", 1000, "2023-06-01")
    employees_db.add_employee(employee)
    token = auth_service.generate_token(employee.id, 600)
    salary = salary_service.get_salary(employee.id, token)
    print(salary)


def test_get_salary_invalid_token():
    with pytest.raises(HTTPException):
        salary = salary_service.get_salary(1, "invalid_token")
