from fastapi import FastAPI, Depends

from auth_service import AuthService
from employeers_bd import EmployeeDB
from salary_service import SalaryService

SECRETKEY = "SECRETKEY"
EXPIRATION_TIME = 60
app = FastAPI()

@app.get("/salary")
def get_salary(employee_id: int, token: str = Depends(AuthService().validate_token)):
    return SalaryService(EmployeeDB(), AuthService(SECRETKEY, EXPIRATION_TIME)).get_salary(employee_id, token)
