class Employee:
    def __init__(self, id, name, salary, next_raise_date):
        self.id = id
        self.name = name
        self.salary = salary
        self.next_raise_date = next_raise_date