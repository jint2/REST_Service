from models import Employee


class EmployeeDB:
    def __init__(self):
        self.employees = {}

    def get_employee(self, employee_id):
        return self.employees.get(employee_id)

    def add_employee(self, employee):
        self.employees[employee.id] = employee


# Создать пример базы данных сотрудников