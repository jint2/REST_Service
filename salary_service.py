from fastapi import HTTPException

from auth_service import AuthService
from employeers_bd import EmployeeDB


class SalaryService:
    def __init__(self, employees_db: EmployeeDB, auth_service: AuthService):
        self.employees_db = employees_db
        self.auth_service = auth_service

    def get_salary(self, employee_id, token: str):

        token = self.auth_service.validate_token(token)
        if employee_id != token:
            raise HTTPException(status_code=401, detail="Invalid token")

        employee = self.employees_db.get_employee(employee_id)
        if not employee:
            raise HTTPException(status_code=404, detail="Employee not found")

        return {"salary": employee.salary}
