from datetime import datetime, timezone

import jwt


class AuthService:

    def __init__(self, secretkey="SECRETKEY", expiration_time=600):
        self.secretkey = secretkey
        self.expiration_time = expiration_time

    def generate_token(self, employee_id, expiration_time):
        expiration_time += datetime.now(tz=timezone.utc).timestamp()
        payload = {"employee_id": employee_id, "exp": expiration_time}
        return jwt.encode(payload, self.secretkey, algorithm="HS256")

    def validate_token(self, token):
        try:
            decoded_payload = jwt.decode(jwt=token, key=self.secretkey, algorithms=["HS256"])
            return decoded_payload["employee_id"]
        except jwt.InvalidTokenError:
            return None
